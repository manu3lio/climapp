package com.manuel_aguilar.climapp.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import com.manuel_aguilar.climapp.Classes.Ciudades;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.Classes.Geocode;
import com.manuel_aguilar.climapp.Classes.Sistema;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.ActivityBase;
import com.manuel_aguilar.climapp.Utils.LeerJson;
import com.master.permissionhelper.PermissionHelper;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import es.dmoral.toasty.Toasty;

public class CiudadesActivity extends ActivityBase {
  PermissionHelper permissionHelper_gps;
  double latitud, longitud;
  String ciudad;
  public static DarkSkyForecast darkSkyForecast = new DarkSkyForecast();
  public Geocode geocode = new Geocode();
  ArrayList<Ciudades> ciudadesArrayList = new ArrayList<>();
  public static Sistema sistema = new Sistema();

  /* Elementos de la vista */
  @BindView(R.id.SP_Ciudades)
  Spinner SP_Ciudades;
  @BindView(R.id.BTN_entrar)
  Button BTN_entrar;
  @BindView(R.id.LL_spiner)
  LinearLayout LL_spiner;

  @Override
  public int provideParentLayoutId() {
    return R.layout.activity_main;
  }

  @Override
  public String provideParentActivityName() {
    return "CiudadesActivity";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Toasty.Config.getInstance().tintIcon(false).apply();
    getCiudadesJSON();
    getSistema();

    permissionHelper_gps = new PermissionHelper(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
    darkSkyForecast = sp.getSavedObjectFromPreference(this, "darkSkyForest", "darkSkyForest", DarkSkyForecast.class);


    SweetAlertDialog sda = new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
            .setTitleText("Ciudades")
            .setContentText("Selecciona opcion para cambio de ciudad")
            .setConfirmText("Por GPS")
            .setConfirmClickListener(sDialog -> {
              sDialog.dismissWithAnimation();
              permiso_gps();
            })
            .showCancelButton(true)
            .setCancelText("Lista ciudades")
            .setCancelClickListener(sDialog -> {
              sDialog.dismissWithAnimation();
              LL_spiner.setVisibility(View.VISIBLE);
            });
    sda.show();


    //  .show();

    BTN_entrar.setOnClickListener(v -> {
      // Compara el tiempo transcurrido contra el ultimo click para evitar doble ejecución con doble click
      if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
        return;
      }
      mLastClickTime = SystemClock.elapsedRealtime();

      // Latitud 0, valor default en la clase Ciudad
      if (latitud == 0.0) {
        Toasty.error(this, "Debe seleccionar una ciudad").show();
      } else {
        // Segun la latitud y longitud se manda la peticion a GeoCode de google para obtener la Lat y Long de la ciudad mas cercana
        hacerRequestGeocode();
      }
    });
  }

  public void getSistema() {
    if (sp.getSavedObjectFromPreference(this, "configuracion", "sistema", Sistema.class) == null) {
      sistema.setSistema("si");
      sistema.setSlug("°C");
      sistema.setLenguaje("es");
    } else {
      sistema = sp.getSavedObjectFromPreference(this, "configuracion", "sistema", Sistema.class);
    }
  }

  public void getCiudadesJSON() {
    LeerJson leerJson = new LeerJson(this, getResources().openRawResource(R.raw.ciudades));
    String json = leerJson.readJson();
    try {
      transformarAObjeto_ciudad(json);
    }
    catch (JSONException e) {
      e.printStackTrace();
    }
  }

  public void transformarAObjeto_ciudad(String json) throws JSONException {
    JSONArray jsonResponse = new JSONArray(json);
    /*Set inicial*/
    Ciudades ciudad_ini = new Ciudades();
    ciudad_ini.setID("0");
    ciudad_ini.setCiudad("Seleccione ciudad...");
    ciudad_ini.setLatitud("0.00");
    ciudad_ini.setLongitud("0.00");
    ciudadesArrayList.add(ciudad_ini);

    for (int i = 0; i < jsonResponse.length(); i++) {
      JSONObject jsonObject = jsonResponse.getJSONObject(i);
      String ID = jsonObject.getString("ID");
      String Ciudad = jsonObject.getString("Ciudad");
      String Latitud = jsonObject.getString("Latitud");
      String Longitud = jsonObject.getString("Longitud");

      Ciudades ciudad = new Ciudades();
      ciudad.setID(ID);
      ciudad.setCiudad(Ciudad);
      ciudad.setLatitud(Latitud);
      ciudad.setLongitud(Longitud);
      ciudadesArrayList.add(ciudad);
    }
    set_spinner_ciudades();
  }

  void set_spinner_ciudades() {
    String[] ITEMS = new String[ciudadesArrayList.size()];
    for (int i = 0; i < ciudadesArrayList.size(); i++) {
      ITEMS[i] = ciudadesArrayList.get(i).getCiudad();
    }
    ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ITEMS);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    SP_Ciudades.setAdapter(adapter);
    SP_Ciudades.setSelection(0, true);
    SP_Ciudades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ciudad = adapterView.getItemAtPosition(i).toString();
        latitud = Double.parseDouble(ciudadesArrayList.get(i).getLatitud());
        longitud = Double.parseDouble(ciudadesArrayList.get(i).getLongitud());
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
        Toasty.error(getBaseContext(), "Debe seleccionar una ciudad").show();
      }
    });
  }

  public void permiso_gps() {
    permissionHelper_gps.request(new PermissionHelper.PermissionCallback() {
      @Override
      public void onPermissionGranted() {
        Log.d(TAG, "onPermissionGranted() called");
        getGpsActual();
      }

      @Override
      public void onPermissionDenied() {
        Log.d(TAG, "onPermissionDenied() called");
        showDialogPermiso();
      }

      @Override
      public void onIndividualPermissionGranted(String[] grantedPermission) {
        Log.d(TAG, "onIndividualPermissionGranted() called: grantedPermission = [" + TextUtils.join(",", grantedPermission) + "]");
      }

      @Override
      public void onPermissionDeniedBySystem() {
        Log.d(TAG, "onPermissionDeniedBySystem() called");
      }
    });
  }

  public void showDialogPermiso() {
    new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("GPS")
            .setContentText("El permiso es necesario para determinar tu ciudad")
            .setConfirmText("Lo aceptaré")
            .setConfirmClickListener(sDialog -> {
              sDialog.dismissWithAnimation();
              permiso_gps();
            })
            .showCancelButton(true)
            .setCancelText("Ciudades")
            .setCancelClickListener(sDialog -> {
              sDialog.cancel();
              LL_spiner.setVisibility(View.VISIBLE);
            })
            .show();
  }

  public void hacerRequestGeocode() {
    showLoading("ClimApp", "Obteniendo informacion...");
    String url = (getString(R.string.URL_geocode) + "latlng=" + latitud + "," + longitud + "&key=" + getString(R.string.KEY_geocode));
    hacerRequest(url, "GET", "responseCiudadGeocode", new JSONObject(), sweetAlertDialog_loading);
  }

  public void hacerRequestDarkSky() {
    String url = getString(R.string.URL_darkSky) + "/" + getString(R.string.KEY_DarkSky) + "/" + latitud + "," + longitud + "?lang=" + sistema.getLenguaje() + "&units=" + sistema.getSistema();
    hacerRequest(url, "GET", "responseCiudad", new JSONObject(), sweetAlertDialog_loading);
  }

  @SuppressLint("MissingPermission")
  public void getGpsActual() {
    LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
    Criteria criteria = new Criteria();
    String bestProvider = locationManager.getBestProvider(criteria, true);
    Location location = locationManager.getLastKnownLocation(bestProvider);

    if (location != null) {
      longitud = location.getLongitude();
      latitud = location.getLatitude();

      if ((!Double.isNaN(longitud)) && (!Double.isNaN(latitud))) {
        Log.d(TAG, "longitud: " + longitud);
        Log.d(TAG, "latitud: " + latitud);
        hacerRequestGeocode();
      } else {
        onFailureGPS();
      }
    } else {
      onFailureGPS();
    }
  }

  public void onFailureGPS() {
    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Error GPS")
            .setContentText("No se pudo obtener tu ubicación GPS, deberás seleccionar tu ciudad más cercana para continuar")
            .setConfirmText("Ok")
            .setConfirmClickListener(sDialog -> {
              sDialog.dismissWithAnimation();
              LL_spiner.setVisibility(View.VISIBLE);
            }).show();
  }

  @Override
  public void onSuccessJson(JSONObject result, String type) {
    switch (type) {
      case "responseCiudad":
        try {
          getInfoFromJSON_DarkSky(result);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        break;

      case "responseCiudadGeocode":
        try {
          getInfoFromJSON_Geocode(result);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        break;
    }
  }

  public void getInfoFromJSON_DarkSky(JSONObject jsonObject) throws JSONException {
    darkSkyForecast.setLatitude(validaCampo(jsonObject, "latitude"));
    darkSkyForecast.setLongitude(validaCampo(jsonObject, "longitude"));
    darkSkyForecast.setTimezone(validaCampo(jsonObject, "timezone"));
    darkSkyForecast.setOffset(Integer.parseInt(validaCampo(jsonObject, "offset")));

    String currently_string = jsonObject.getString("currently");
    String hourly_string = jsonObject.getString("hourly");
    String daily_string = jsonObject.getString("daily");

    darkSkyForecast.setCurrently(gson.fromJson(currently_string, DarkSkyForecast.DataClima.class));
    darkSkyForecast.setHourly(gson.fromJson(hourly_string, DarkSkyForecast.DataClimaInfo.class));
    darkSkyForecast.setDaily(gson.fromJson(daily_string, DarkSkyForecast.DataClimaInfo.class));

    /*Asignar valores de geoCoding*/
    darkSkyForecast.setGlobal_code_geocode(geocode.getPlus_code().getGlobal_code());
    darkSkyForecast.setCiudad(ciudad);

    hideLoading_cargando();
    sp.saveObjectToSharedPreference(this, "darkSkyForest", "darkSkyForest", darkSkyForecast);

    //Regresar a Principal una vez que se guardo el resultado
    Intent returnIntent = getIntent();
    setResult(11, returnIntent);
    finish();
  }

  public void getInfoFromJSON_Geocode(JSONObject jsonObject) throws JSONException {
    geocode.setStatus(validaCampo(jsonObject, "status"));
    String plus_code_string = jsonObject.getString("plus_code");
    geocode.setPlus_code(gson.fromJson(plus_code_string, Geocode.PlusCode.class));
    getNameCiudad(geocode.getPlus_code().getCompound_code());
    hacerRequestDarkSky();
  }

  public void getNameCiudad(String vc_ciudad) {
    // Example: RJ48+3J Culiacán Rosales, Sin., México";
    String[] nombreArray = vc_ciudad.split(" ");
    ciudad = nombreArray[1].replace(",", "");
  }

  public String validaCampo(JSONObject jsonObject, String campo) {
    String regreso = "";
    if (jsonObject.has(campo)) {
      try {
        regreso = jsonObject.getString(campo);
      }
      catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return regreso;
  }

  @Override
  public void onSuccessJson_Array(JSONArray result, String type) {
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (permissionHelper_gps != null) {
      permissionHelper_gps.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }
}
