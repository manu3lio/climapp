package com.manuel_aguilar.climapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.navigation.NavigationView;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.Classes.Sistema;
import com.manuel_aguilar.climapp.Fragments.InicioFragment;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.ActivityBase;
import com.manuel_aguilar.climapp.Utils.RequestVolley.PostVolleyJsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;


public class PrincipalActivity extends ActivityBase implements NavigationView.OnNavigationItemSelectedListener {
  public static Toolbar toolbar;
  public static ImageView back;
  public static TextView textoHeader;
  DrawerLayout drawer;
  NavigationView navigationView;
  ActionBarDrawerToggle toggle;
  public static long mLastClickTime = 0;
  public static DarkSkyForecast darkSkyForecast = new DarkSkyForecast();
  public static Sistema sistema = new Sistema();
  SwipeRefreshLayout SwipeRefreshFragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    darkSkyForecast = sp.getSavedObjectFromPreference(this, "darkSkyForest", "darkSkyForest", DarkSkyForecast.class);
    sistema = sp.getSavedObjectFromPreference(this, "configuracion", "sistema", Sistema.class);

    setToolbar();
    setDrawerLayout();
    setNavigationView();
    setFirstFragment();
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    drawer.addDrawerListener(toggle);
    toggle.syncState();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    toggle.onConfigurationChanged(newConfig);
  }

  public void setNavigationView() {
    navigationView = findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    setInfoHeaderNavigation();
  }

  public void setInfoHeaderNavigation() {
    View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_principal);
    TextView nombreCiudad = headerLayout.findViewById(R.id.TV_nombre_ciudad);
    TextView SummaryClima = headerLayout.findViewById(R.id.TV_summary_clima);
    ImageView imagenClima = headerLayout.findViewById(R.id.IV_logo);
    ImageView imagenBackClima = headerLayout.findViewById(R.id.IV_background_img);

    nombreCiudad.setText("ClimApp");
    SummaryClima.setText(darkSkyForecast.getCiudad());
    switch (darkSkyForecast.getCurrently().getIcon()) {
      case "clear-day":
        imagenBackClima.setImageDrawable(getDrawable(R.drawable.clear_day));
        break;
      case "clear-night":
        imagenBackClima.setImageDrawable(getDrawable(R.drawable.clear_night));
        break;
      case "rain":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.rain));
        break;
      case "snow":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.snow));
        break;
      case "sleet":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.sleet));
        break;
      case "fog":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.fog));
        break;
      case "partly-cloudy-night":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.partly_cloudy_night));
        break;
      case "wind":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.wind));
        break;
      case "cloudy":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.cloudy));
        break;
      case "partly-cloudy-day":
        imagenBackClima.setImageDrawable(getResources().getDrawable(R.drawable.partly_cloudy_day));
        break;
    }
  }

  @Override
  public void onBackPressed() {
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
    int count = getFragmentManager().getBackStackEntryCount();
    Log.d(TAG, "onBackPressed: " + count);
    if (count == 0) {
      Log.e(TAG, "primer fragment");
//      super.onBackPressed();
//			finishAffinity();
//			android.os.Process.killProcess(android.os.Process.myPid());
    } else {
      getFragmentManager().popBackStack();
    }
  }

  @Override
  public int provideParentLayoutId() {
    return R.layout.activity_principal;
  }

  @Override
  public String provideParentActivityName() {
    return "PrincipalActivity";
  }

  void setToolbar() {
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    getSupportActionBar().setDisplayShowHomeEnabled(false);

    textoHeader = findViewById(R.id.TV_header_appbar);
  }

  void setDrawerLayout() {
    drawer = findViewById(R.id.drawer_layout);
    toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
      public void onDrawerClosed(View view) {
        super.onDrawerClosed(view);
      }

      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
      }
    };

    drawer.setDrawerListener(toggle);
    toggle.setDrawerIndicatorEnabled(false);
    toggle.setHomeAsUpIndicator(R.drawable.ic_menu_toolbar);
    toggle.setToolbarNavigationClickListener(v -> {
      if (drawer.isDrawerVisible(GravityCompat.START)) {
        drawer.closeDrawer(GravityCompat.START);
      } else {
        drawer.openDrawer(GravityCompat.START);
      }
    });
  }

  public void setFirstFragment() {
    InicioFragment inicioFragment = new InicioFragment();
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
    fragmentTransaction.replace(R.id.framelayoutContainer, inicioFragment, "inicioFragment");
    fragmentTransaction.commit();
  }

  public void hacerRequestDarkSky_Fragments(SwipeRefreshLayout SwipeRefresh) {
    SwipeRefreshFragment = SwipeRefresh;
    String url = getString(R.string.URL_darkSky) + "/" + getString(R.string.KEY_DarkSky) + "/" + darkSkyForecast.getLatitude() + "," + darkSkyForecast.getLongitude() + "?lang=" + sistema.getLenguaje() + "&units=" + sistema.getSistema();

    // validar que cuente con conexion a internet, si no poner el swipeRefresh en falso
    PostVolleyJsonRequest postVolleyJsonRequest = new PostVolleyJsonRequest(this);
    if (postVolleyJsonRequest.setNetworkListener()) {
      hacerRequest(url, "GET", "responseCiudad", new JSONObject(), sweetAlertDialog_loading);
    }
    else{
      postVolleyJsonRequest.showNoCnx();
      SwipeRefreshFragment.setRefreshing(false);
    }
  }

  public void getInfoFromJSON_DarkSky_Fragments(Context context, JSONObject jsonObject) throws JSONException {
    darkSkyForecast.setLatitude(validaCampo(jsonObject, "latitude"));
    darkSkyForecast.setLongitude(validaCampo(jsonObject, "longitude"));
    darkSkyForecast.setTimezone(validaCampo(jsonObject, "timezone"));

    String currently_string = jsonObject.getString("currently");
    String hourly_string = jsonObject.getString("hourly");
    String daily_string = jsonObject.getString("daily");

    darkSkyForecast.setCurrently(gson.fromJson(currently_string, DarkSkyForecast.DataClima.class));
    darkSkyForecast.setHourly(gson.fromJson(hourly_string, DarkSkyForecast.DataClimaInfo.class));
    darkSkyForecast.setDaily(gson.fromJson(daily_string, DarkSkyForecast.DataClimaInfo.class));

    sp.saveObjectToSharedPreference(context, "darkSkyForest", "darkSkyForest", darkSkyForecast);
    SwipeRefreshFragment.setRefreshing(false);
    Toasty.success(this, "Información actualizada").show();

  }

  public static String validaCampo(JSONObject jsonObject, String campo) {
    String regreso = "";
    if (jsonObject.has(campo)) {
      try {
        regreso = jsonObject.getString(campo);
      }
      catch (JSONException e) {
        e.printStackTrace();
      }
    }
    return regreso;
  }

  @Override
  public void onSuccessJson(JSONObject result, String type) {
    switch (type) {
      case "responseCiudad":
        try {
          getInfoFromJSON_DarkSky_Fragments(this, result);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        break;
    }
  }

  @Override
  public void onSuccessJson_Array(JSONArray result, String type) {

  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();
    switch (id) {
      case R.id.nav_ciudades:
        Intent intent = new Intent(getApplicationContext(), CiudadesActivity.class);
        startActivityForResult(intent, 1111);
        break;

      case R.id.nav_configuracion:
        Intent intent2 = new Intent(getApplicationContext(), ConfiguracionActivity.class);
        startActivityForResult(intent2, 2222);
        break;
    }
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d(TAG, "onActivityResult");
    Log.d(TAG, "resultCode: " + resultCode);
    Log.d(TAG, "requestCode: " + requestCode);

    switch (requestCode) {
      case 1111:
        if (resultCode == 11) {
          recreate();
        }
        break;

      case 2222:
        if (resultCode == 11) {
          finish();
          Intent intent = new Intent(this, MainActivity.class);
          sp.deleteFileFromPreference(this, "darkSkyForest");
          startActivity(intent);
        }
    }
  }
}
