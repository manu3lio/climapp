package com.manuel_aguilar.climapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.manuel_aguilar.climapp.Classes.Sistema;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.ActivityBase;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import es.dmoral.toasty.Toasty;

public class ConfiguracionActivity extends ActivityBase {
  @BindView(R.id.SP_Idioma) Spinner SP_Idioma;
  @BindView(R.id.SP_sistema) Spinner SP_sistema;
  @BindView(R.id.BTN_guardar)
  Button BTN_guardar;
  public Sistema sistema = new Sistema();
  String idioma;
  String sist;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    sistema = sp.getSavedObjectFromPreference(this, "configuracion", "sistema", Sistema.class);

    set_spinner_idiomas();
    set_spinner_sistema();

    BTN_guardar.setOnClickListener(view -> {
      Log.d(TAG, "idioma: " + idioma);
      Log.d(TAG, "sist: " + sist);

      if (idioma != null && sist != null) {
        sistema.setLenguaje(idioma);
        sistema.setSistema(sist);

        Log.d(TAG, "onCreate: " + gson.toJson(sistema));
        sp.saveObjectToSharedPreference(this, "configuracion", "sistema", sistema);

        //Regresar a Principal una vez que se guardo el resultado
        Intent returnIntent = getIntent();
        setResult(11, returnIntent);
        finish();
      }
      else{
        Toasty.error(this, "No ha modificado los valores de idioma y sistema").show();
      }

    });
  }

  @Override
  public int provideParentLayoutId() {
    return R.layout.activity_configuracion;
  }

  @Override
  public String provideParentActivityName() {
    return "ConfiguracionActivity";
  }

  void set_spinner_idiomas() {
    String[] array = sistema.getArrayLenguajes();
    String[] ITEMS = new String[array.length + 1];
    for (int i = 0; i < array.length; i++) {
      ITEMS[i] = array[i];
    }
    ITEMS[ITEMS.length - 1] = "Selecccione Idioma";

    ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ITEMS);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    SP_Idioma.setAdapter(adapter);
    SP_Idioma.setSelection(ITEMS.length - 1, true);
    SP_Idioma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        idioma = adapterView.getItemAtPosition(i).toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
        Toasty.error(getBaseContext(), "Debe seleccionar una ciudad").show();
      }
    });
  }

  void set_spinner_sistema() {
    String[] array = sistema.getArraySistemas();
    String[] ITEMS = new String[array.length + 1];
    for (int i = 0; i < array.length; i++) {
      ITEMS[i] = array[i];
    }
    ITEMS[ITEMS.length - 1] = "Selecccione sistema";

    ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ITEMS);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    SP_sistema.setAdapter(adapter);
    SP_sistema.setSelection(ITEMS.length - 1, true);
    SP_sistema.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        sist = adapterView.getItemAtPosition(i).toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
        Toasty.error(getBaseContext(), "Debe seleccionar una ciudad").show();
      }
    });
  }


  @Override
  public void onSuccessJson(JSONObject result, String type) {

  }

  @Override
  public void onSuccessJson_Array(JSONArray result, String type) {

  }
}
