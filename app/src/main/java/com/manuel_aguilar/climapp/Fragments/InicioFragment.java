package com.manuel_aguilar.climapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.manuel_aguilar.climapp.Adapters.Viewpager_Clima_Adapter;
import com.manuel_aguilar.climapp.R;

import java.util.ArrayList;

public class InicioFragment extends Fragment {
  private static final String TAG = "InicioFragment";
  public View view;
  public Intent intent;
  public static ViewPager viewPager;
  public static TabLayout tabLayout;
  public static ArrayList<String> nombresTabs = new ArrayList<>();

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    nombresTabs.clear();
    nombresTabs.add("Actual");
    nombresTabs.add("Hora");
    nombresTabs.add("Semana");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.layout_viewpager_tablayout, container, false);
    intent = getActivity().getIntent();
    viewPager = view.findViewById(R.id.viewpager);
    tabLayout = view.findViewById(R.id.appbartabs);
    tabLayout.setTabMode(TabLayout.MODE_FIXED);

    setViewPager();
    setTabLayout();
    return view;
  }

  public void setViewPager() {
    Viewpager_Clima_Adapter viewPagerClima = new Viewpager_Clima_Adapter(getChildFragmentManager(), nombresTabs);
    viewPager.setAdapter(viewPagerClima);
    viewPager.setOffscreenPageLimit(nombresTabs.size() + 1);
    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageSelected(int position) {
        Log.d(TAG, "onPageSelected: " + position);
        tabLayout.getTabAt(position).select();
      }

      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
      }

      @Override
      public void onPageScrollStateChanged(int state) {
      }
    });
  }

  public void setTabLayout() {
    for (int i = 0; i < nombresTabs.size(); i++) {
      tabLayout.addTab(tabLayout.newTab().setText(nombresTabs.get(i)));
    }
    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        Log.e(TAG, "onTabSelected: " + nombresTabs.get(tab.getPosition()));
        viewPager.setCurrentItem(tab.getPosition());
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {
      }
    });

  }

}
