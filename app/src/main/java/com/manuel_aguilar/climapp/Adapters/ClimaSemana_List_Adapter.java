package com.manuel_aguilar.climapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.SharedPreferencesUtils;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.sistema;

public class ClimaSemana_List_Adapter extends BaseAdapter {
  private static final String TAG = "ClimaSemana_List_Ada";
  public Gson gson = new GsonBuilder().serializeNulls().create();
  public SharedPreferencesUtils sp = new SharedPreferencesUtils();
  Context context;
  DarkSkyForecast darkSkyForecast;
  ArrayList<DarkSkyForecast.DataClima> arrayListElemts;

  public ClimaSemana_List_Adapter(Context context, DarkSkyForecast darkSkyForecast,  ArrayList<DarkSkyForecast.DataClima> arrayListElemts) {
    this.context = context;
    this.darkSkyForecast = darkSkyForecast;
    this.arrayListElemts = arrayListElemts;
  }

  public void refreshData(ArrayList<DarkSkyForecast.DataClima> arrayListElemts) {
    this.arrayListElemts.clear();
    this.arrayListElemts.addAll(arrayListElemts);
    notifyDataSetChanged();
  }

  @Override
  public int getCount() {
    if (arrayListElemts == null) {
      return 0;
    }
    return arrayListElemts.size();
  }

  @Override
  public Object getItem(int i) {
    return arrayListElemts.get(i);
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    ViewHolder viewHolder;
    String Objeto = gson.toJson(arrayListElemts.get(i));
    DarkSkyForecast.DataClima itemDataClimaInfo = gson.fromJson(Objeto, DarkSkyForecast.DataClima.class);
//
    if (view == null) {
      view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_daily_lista_item, viewGroup, false);
      viewHolder = new ViewHolder();
      view.setHapticFeedbackEnabled(true);
      viewHolder.CV_icon_clima = (CircleImageView) view.findViewById(R.id.CV_icon_clima);
      viewHolder.TV_date_clima = (TextView) view.findViewById(R.id.TV_date_clima);
      viewHolder.TV_temperatureMin_clima = (TextView) view.findViewById(R.id.TV_temperatureMin_clima);
      viewHolder.TV_temperatureMax_clima = (TextView) view.findViewById(R.id.TV_temperatureMax_clima);
      viewHolder.TV_grade_daily_clima = (TextView) view.findViewById(R.id.TV_grade_daily_clima);
      view.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) view.getTag();
    }

    Long time = itemDataClimaInfo.getTime() * 1000;
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(time);
    TimeZone timeZone = TimeZone.getTimeZone(darkSkyForecast.getTimezone());
    calendar.setTimeZone(timeZone);

    // Dar formato de la fecha
    DateFormat formatter = new SimpleDateFormat("EEE dd/MMM/yyyy", Locale.getDefault());

    viewHolder.TV_date_clima.setText(formatter.format(calendar.getTime()));

    String nombreIcono = itemDataClimaInfo.getIcon().replace("-", "_");
    int intImagen = getResourceID(nombreIcono, "drawable", context);
    Picasso.get().load(intImagen).into(viewHolder.CV_icon_clima);

    viewHolder.TV_temperatureMin_clima.setText(itemDataClimaInfo.getTemperatureMin()+"");
    viewHolder.TV_temperatureMax_clima.setText(itemDataClimaInfo.getTemperatureMax()+"");

    viewHolder.TV_grade_daily_clima.setText(" " + sistema.getSlug());

    return view;
  }

  static class ViewHolder {
    CircleImageView CV_icon_clima;
    TextView TV_date_clima;
    TextView TV_temperatureMin_clima;
    TextView TV_temperatureMax_clima;
    TextView TV_grade_daily_clima;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
    final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
    if (ResourceID == 0) {
      throw new IllegalArgumentException("No resource string found with name " + resName);
    } else {
      return ResourceID;
    }
  }
}
