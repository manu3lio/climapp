package com.manuel_aguilar.climapp.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.manuel_aguilar.climapp.Fragment_tabs.ClimaActualFragmentTab;
import com.manuel_aguilar.climapp.Fragment_tabs.ClimaHoraFragmentTab;
import com.manuel_aguilar.climapp.Fragment_tabs.ClimaSemanaFragmentTab;

import java.util.ArrayList;

public class Viewpager_Clima_Adapter extends FragmentPagerAdapter {
  private ArrayList<String> tags;

  public Viewpager_Clima_Adapter(FragmentManager fm, ArrayList<String> tags) {
    super(fm);
    this.tags = tags;
  }

  @Override
  public int getCount() {
    return tags.size();
  }

  @Override
  public Fragment getItem(int position) {
    Fragment f = new Fragment();
    switch (position){
      case 0:
        f = new ClimaActualFragmentTab();
        break;
      case 1:
        f = new ClimaHoraFragmentTab();
        break;
      case 2:
        f = new ClimaSemanaFragmentTab();
        break;
    }
    return f;
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return tags.get(position);
  }
}