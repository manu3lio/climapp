package com.manuel_aguilar.climapp.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.Classes.Sistema;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.SharedPreferencesUtils;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.sistema;

public class ClimaHora_List_Adapter extends BaseAdapter {
  private static final String TAG = "ClimaHora_List_Adapter";
  public Gson gson = new GsonBuilder().serializeNulls().create();
  public SharedPreferencesUtils sp = new SharedPreferencesUtils();
  Context context;
  DarkSkyForecast darkSkyForecast;
  ArrayList<DarkSkyForecast.DataClima> arrayListElemts;

  public ClimaHora_List_Adapter(Context context, DarkSkyForecast darkSkyForecast,  ArrayList<DarkSkyForecast.DataClima> arrayListElemts) {
    this.context = context;
    this.darkSkyForecast = darkSkyForecast;
    this.arrayListElemts = arrayListElemts;
  }

  public void refreshData(ArrayList<DarkSkyForecast.DataClima> arrayListElemts) {
    this.arrayListElemts.clear();
    this.arrayListElemts.addAll(arrayListElemts);
    notifyDataSetChanged();
  }

  @Override
  public int getCount() {
    if (arrayListElemts == null) {
      return 0;
    }
    return arrayListElemts.size();
  }

  @Override
  public Object getItem(int i) {
    return arrayListElemts.get(i);
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    ViewHolder viewHolder;
    String Objeto = gson.toJson(arrayListElemts.get(i));
    DarkSkyForecast.DataClima itemDataClimaInfo = gson.fromJson(Objeto, DarkSkyForecast.DataClima.class);
//
    if (view == null) {
      view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_hourly_lista_item, viewGroup, false);
      viewHolder = new ViewHolder();
      view.setHapticFeedbackEnabled(true);
      viewHolder.CV_icon_clima = (CircleImageView) view.findViewById(R.id.CV_icon_clima);
      viewHolder.TV_time_clima = (TextView) view.findViewById(R.id.TV_time_clima);
      viewHolder.TV_temperature_clima = (TextView) view.findViewById(R.id.TV_temperature_clima);
      viewHolder.TV_grade_clima = (TextView) view.findViewById(R.id.TV_grade_clima);
      viewHolder.TV_precipProbability_clima = (TextView) view.findViewById(R.id.TV_precipProbability_clima);
      view.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) view.getTag();
    }

    Long time = itemDataClimaInfo.getTime() * 1000;
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(time);
    TimeZone timeZone = TimeZone.getTimeZone(darkSkyForecast.getTimezone());
    calendar.setTimeZone(timeZone);

    // Formateo para solo obtener la hora
    DateFormat formatter = new SimpleDateFormat("EEE dd/MMM HH:mm", Locale.getDefault());

    // viewHolder.TV_time_clima.setText(calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
    viewHolder.TV_time_clima.setText(formatter.format(calendar.getTime()));

    String nombreIcono = itemDataClimaInfo.getIcon().replace("-", "_");
    int intImagen = getResourceID(nombreIcono, "drawable", context);
    Picasso.get().load(intImagen).into(viewHolder.CV_icon_clima);

    viewHolder.TV_temperature_clima.setText(itemDataClimaInfo.getTemperature() + "");
    viewHolder.TV_grade_clima.setText(" " + sistema.getSlug());
    viewHolder.TV_precipProbability_clima.setText( Math.round(itemDataClimaInfo.getPrecipProbability() * 100) + "%");

    return view;
  }

  static class ViewHolder {
    CircleImageView CV_icon_clima;
    TextView TV_time_clima;
    TextView TV_temperature_clima;
    TextView TV_grade_clima;
    TextView TV_precipProbability_clima;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
    final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
    if (ResourceID == 0) {
      throw new IllegalArgumentException("No resource string found with name " + resName);
    } else {
      return ResourceID;
    }
  }
}
