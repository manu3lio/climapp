package com.manuel_aguilar.climapp.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.manuel_aguilar.climapp.Utils.RequestVolley.PostVolleyJsonRequest;
import com.manuel_aguilar.climapp.Utils.RequestVolley.VolleyJsonRespondsListener;
import com.manuel_aguilar.climapp.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONObject;

import butterknife.ButterKnife;


public abstract class ActivityBase extends AppCompatActivity implements VolleyJsonRespondsListener {
  public static String TAG = "";
  public static SharedPreferencesUtils sp = new SharedPreferencesUtils();
  public static Gson gson = new GsonBuilder().serializeNulls().create();
  public static SweetAlertDialog sweetAlertDialog_loading;
  public static long mLastClickTime = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(provideParentLayoutId());
    ButterKnife.bind(this);
    sweetAlertDialog_loading = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
    TAG = provideParentActivityName();
  }

  public void showLoading(String tittle, String message) {
    sweetAlertDialog_loading.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
    sweetAlertDialog_loading.setTitleText(tittle);
    sweetAlertDialog_loading.setContentText(message);
    sweetAlertDialog_loading.setCancelable(false);
    sweetAlertDialog_loading.show();
  }

  public void hideLoading_cargando() {
    if (sweetAlertDialog_loading.isShowing()) {
      sweetAlertDialog_loading.dismiss();
    }
  }

  public void alertBuilder_info(String mensaje) {
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    alertDialogBuilder.setMessage(mensaje);
    alertDialogBuilder.setNeutralButton("Ok", (dialogInterface, i) -> {
       dialogInterface.dismiss();
    });
    alertDialogBuilder.show();
  }

  public abstract int provideParentLayoutId();

  public abstract String provideParentActivityName();

  public void nextActivity(Class actividad, boolean fin) {
    Intent intent = new Intent(getApplicationContext(), actividad);
    if (fin)
      this.finish();

    startActivity(intent);
  }

  public void hacerRequest(String url, String requestType, String responseName, JSONObject jsonObjectParams, SweetAlertDialog sweetAlertDialog) {
    try {

      /* ejemplo JSONObject parames
      JSONObject params = new JSONObject();
      params.put("id_cliente", id_cliente);
      params.put("id_uuid", "Android");
      params.put("dispositivo", "Android");
      */
      new PostVolleyJsonRequest(this, this, requestType, responseName, url, jsonObjectParams, sweetAlertDialog);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onFailureJson(int responseCode, String responseMessage) {
    Log.e(TAG, "onFailureJson: responseCode:" + responseCode);
    Log.e(TAG, "onFailureJson: responseMessage:" +responseMessage );

    String msj = "Ocurrió un error al solicitar los datos, intente de nuevo más tarde...";
    if(responseMessage.length()>5){
      msj = responseMessage;
    }

    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setTitleText("Error")
            .setContentText(msj)
            .setConfirmText("Cerrar")
            .setConfirmClickListener(SweetAlertDialog::dismissWithAnimation)
            .show();

    hideLoading_cargando();
  }
}

