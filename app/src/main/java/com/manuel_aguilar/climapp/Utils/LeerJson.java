package com.manuel_aguilar.climapp.Utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LeerJson {
	Context context;
	InputStream JSON_Ref;

	public LeerJson(Context current, InputStream JSON_Ref){
		this.context = current;
		this.JSON_Ref = JSON_Ref;
	}

	public String readJson(){
		StringBuffer sb = new StringBuffer();
		BufferedReader br = null;

		try{
			br = new BufferedReader(new InputStreamReader(JSON_Ref));
			String line;

			while((line = br.readLine()) != null){
				sb.append(line);
			}

		}
		catch (IOException e){
			e.printStackTrace();
		}
		finally {
			try{
				br.close();
			}
			catch (IOException e){
				e.printStackTrace();
			}
		}

		return sb.toString();
	}


}