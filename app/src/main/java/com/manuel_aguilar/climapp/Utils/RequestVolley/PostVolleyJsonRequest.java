package com.manuel_aguilar.climapp.Utils.RequestVolley;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.manuel_aguilar.climapp.R;
import com.manuel_aguilar.climapp.Utils.ActivityBase;
import com.manuel_aguilar.climapp.Utils.SharedPreferencesUtils;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class PostVolleyJsonRequest {
    private static final String TAG = "PostVolleyJsonRequest" ;
    private String type;
    private Context context;
    private Activity act;
    private VolleyJsonRespondsListener volleyJsonRespondsListener;
    private String networkurl;
    private JSONObject jsonObject = null;
    private JSONObject params;
    private String token;
    private String request;
    SharedPreferencesUtils sp = new SharedPreferencesUtils();
    private static final int MY_SOCKET_TIMEOUT_MS = 600000;
    private static String errorInternet = "No se cuenta con conexión a internet";
    private static String errorPeticion = "Error de conexión";

    public PostVolleyJsonRequest( Activity act) {
        this.act = act;
    }
    //  ALERTAS
    public boolean setNetworkListener( ) {
        ConnectivityManager cm = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            Log.d(TAG, "setNetworkListener: Conectado");
            return true;
        } else {
            Log.d(TAG, "setNetworkListener: NO Conectado");
            return false;
        }
    }
    public void showNoCnx() {
        Toasty.custom(act, errorInternet, R.drawable.ic_error_outline_white_48dp, act.getResources().getColor(R.color.Red_warning), Toast.LENGTH_SHORT, true, true).show();
    }
    public void showError() {
        Toasty.custom(act, errorPeticion, R.drawable.ic_error_outline_white_48dp, act.getResources().getColor(R.color.Red_warning), Toast.LENGTH_SHORT, true, true).show();
    }
    public void showError(String error) {
        Toasty.custom(act, error, R.drawable.ic_error_outline_white_48dp, act.getResources().getColor(R.color.Red_warning), Toast.LENGTH_SHORT, true, true).show();
    }
    public void showSuccess(String texto) {
        Toasty.custom(act, texto, R.drawable.ic_error_outline_white_48dp, act.getResources().getColor(R.color.colorPrimary), Toast.LENGTH_SHORT, true, true).show();
    }

    public PostVolleyJsonRequest(Activity act, ActivityBase volleyJsonRespondsListener, String request, String type, String netnetworkUrl, JSONObject params, SweetAlertDialog sweetAlertDialog) {
        this.act = act;
        this.volleyJsonRespondsListener = volleyJsonRespondsListener;
        this.request = request;
        this.type = type;
        this.networkurl = netnetworkUrl;
        this.params = params;
        this.token = sp.getSavedObjectFromPreference(act, "profile", "token", String.class);

        if(setNetworkListener( ) ) {
            switch (request) {
                case "POST":
                    sendRequest_POST();
                    break;
                case "GET":
                    sendRequest_GET();
                    break;
                case "GET_ARRAY":
                    sendRequest_GET_Array();
                    break;
                default:
                    showError("Error en método de petición");
                    sweetAlertDialog.dismiss();
                    break;
            }
        }
        else{
            showNoCnx();
            sweetAlertDialog.dismiss();
        }
    }

    private void sendRequest_GET_Array() {
        Log.d("url", "" + networkurl);
        Log.d("method: ", " GET_ARRAY");
        Log.d("params", "" + params);
        Log.d("token", "" + token);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(networkurl,
                response -> volleyJsonRespondsListener.onSuccessJson_Array(response, type),
                error -> VolleyLog.d(TAG, "Error: " + error.getMessage()))
        {
            @Override
            public Map<String, String> getHeaders () {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestqueue = Volley.newRequestQueue(act);
        requestqueue.add(jsonArrayRequest);
    }

    private void sendRequest_GET() {
        Log.d("url", "" + networkurl);
        Log.d("method: ", " GET");
        Log.d("params", "" + params);
        Log.d("token", "" + token);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, networkurl, params,
                response -> {
                    Log.e("response", "response " + response);
                    volleyJsonRespondsListener.onSuccessJson(response, type);
                },
                error -> {
                    try {
                        showError();
                        NetworkResponse response = error.networkResponse;
                        Log.e("error ", "response " + response);
                        if (response != null) {
                            int code = response.statusCode;
                            String errorMsg = new String(response.data);
                            Log.e("error ", "response" + errorMsg);
                            try {
                                jsonObject = new JSONObject(errorMsg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String msg = jsonObject.optString("message");
                            volleyJsonRespondsListener.onFailureJson(code, msg);
                        } else {
                            String errorMsg = error.getMessage();
                            volleyJsonRespondsListener.onFailureJson(0, errorMsg);
                        }
                    } catch (Exception e) {
                        showError();
                        e.printStackTrace();
                        Log.e("Exception", "e message: " + e.getMessage());
                        Log.e("Exception", "e cause: " + e.getCause());
                        Log.e("Exception", "e localizedMessage: " + e.getLocalizedMessage());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders () {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", token);
                return headers;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestqueue = Volley.newRequestQueue(act);
        requestqueue.add(jsObjRequest);

    }

    private void sendRequest_POST() {
        Log.d("url: ", "" + networkurl);
        Log.d("method: ", " POST");
        Log.d("params: ", "" + params);
        Log.d("token: ", "" + token);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, networkurl, params,
                response -> {
                    Log.e("response", "response " + response);
                    volleyJsonRespondsListener.onSuccessJson(response, type);
                },
                error -> {
                    try {
                        showError();
                        NetworkResponse response = error.networkResponse;
                        Log.e("response", "response " + response);
                        if (response != null) {
                            int code = response.statusCode;

                            String errorMsg = new String(response.data);
                            Log.e("response", "response" + errorMsg);
                            try {
                                jsonObject = new JSONObject(errorMsg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String msg = jsonObject.optString("message");
                            volleyJsonRespondsListener.onFailureJson(code, msg);
                        } else {
                            String errorMsg = error.getMessage();
                            volleyJsonRespondsListener.onFailureJson(0, errorMsg);
                        }
                    } catch (Exception e) {
                        showError();
                        Log.e("Exception", "e message: " + e.getMessage());
                        Log.e("Exception", "e cause: " + e.getCause());
                        Log.e("Exception", "e localizedMessage: " + e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders () {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", token);
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestqueue = Volley.newRequestQueue(act);
        requestqueue.add(jsObjRequest);
    }

}