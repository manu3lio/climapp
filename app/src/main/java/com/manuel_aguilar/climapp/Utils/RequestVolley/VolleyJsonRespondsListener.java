package com.manuel_aguilar.climapp.Utils.RequestVolley;


import org.json.JSONArray;
import org.json.JSONObject;

public interface VolleyJsonRespondsListener {

    public void onSuccessJson(JSONObject result, String type);

    public void onSuccessJson_Array(JSONArray result, String type);

    public void onFailureJson(int responseCode, String responseMessage);
}
