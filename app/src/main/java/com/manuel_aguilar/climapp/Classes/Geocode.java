package com.manuel_aguilar.climapp.Classes;

public class Geocode {
  PlusCode plus_code;
  String status;

  public class PlusCode{
    String compound_code;
    String global_code;

    public String getCompound_code() {
      return compound_code;
    }

    public void setCompound_code(String compound_code) {
      this.compound_code = compound_code;
    }

    public String getGlobal_code() {
      return global_code;
    }

    public void setGlobal_code(String global_code) {
      this.global_code = global_code;
    }
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public PlusCode getPlus_code() {
    return plus_code;
  }

  public void setPlus_code(PlusCode plus_code) {
    this.plus_code = plus_code;
  }
}
