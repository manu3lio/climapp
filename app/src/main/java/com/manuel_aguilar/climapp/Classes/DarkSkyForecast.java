package com.manuel_aguilar.climapp.Classes;

import java.util.ArrayList;

public class DarkSkyForecast {
  String global_code_geocode;
  String ciudad;
  String latitude, longitude;
  String timezone;
  DataClima currently;
  DataClimaInfo hourly;
  DataClimaInfo daily;
  int offset;

  public static class DataClima {
    long time;
    String summary;
    String icon;
    double precipProbability;
    String precipType;
    double temperature;
    double apparentTemperature;
    double temperatureMax;
    double temperatureMin;
    double humidity;
    int uvIndex;
    double visibility;
    double windSpeed;

    public double getVisibility() {
      return visibility;
    }

    public void setVisibility(double visibility) {
      this.visibility = visibility;
    }

    public double getWindSpeed() {
      return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
      this.windSpeed = windSpeed;
    }

    public long getTime() {
      return time;
    }

    public void setTime(long time) {
      this.time = time;
    }

    public String getSummary() {
      return summary;
    }

    public void setSummary(String summary) {
      this.summary = summary;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public double getPrecipProbability() {
      return precipProbability;
    }

    public void setPrecipProbability(double precipProbability) {
      this.precipProbability = precipProbability;
    }

    public String getPrecipType() {
      return precipType;
    }

    public void setPrecipType(String precipType) {
      this.precipType = precipType;
    }

    public double getTemperature() {
      return temperature;
    }

    public void setTemperature(double temperature) {
      this.temperature = temperature;
    }

    public double getApparentTemperature() {
      return apparentTemperature;
    }

    public void setApparentTemperature(double apparentTemperature) {
      this.apparentTemperature = apparentTemperature;
    }

    public double getTemperatureMax() {
      return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
      this.temperatureMax = temperatureMax;
    }

    public double getTemperatureMin() {
      return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
      this.temperatureMin = temperatureMin;
    }

    public double getHumidity() {
      return humidity;
    }

    public void setHumidity(double humidity) {
      this.humidity = humidity;
    }

    public int getUvIndex() {
      return uvIndex;
    }

    public void setUvIndex(int uvIndex) {
      this.uvIndex = uvIndex;
    }
  }
  public static class DataClimaInfo {
    String summary;
    String icon;
    ArrayList<DataClima> data;

    public String getSummary() {
      return summary;
    }

    public void setSummary(String summary) {
      this.summary = summary;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public ArrayList<DataClima> getData() {
      return data;
    }

    public void setData(ArrayList<DataClima> data) {
      this.data = data;
    }
  }

  public String getGlobal_code_geocode() {
    return global_code_geocode;
  }

  public void setGlobal_code_geocode(String global_code_geocode) {
    this.global_code_geocode = global_code_geocode;
  }

  public String getCiudad() {
    return ciudad;
  }

  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  public DataClima getCurrently() {
    return currently;
  }

  public void setCurrently(DataClima currently) {
    this.currently = currently;
  }

  public DataClimaInfo getHourly() {
    return hourly;
  }

  public void setHourly(DataClimaInfo hourly) {
    this.hourly = hourly;
  }

  public DataClimaInfo getDaily() {
    return daily;
  }

  public void setDaily(DataClimaInfo daily) {
    this.daily = daily;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }
}

