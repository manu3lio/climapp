package com.manuel_aguilar.climapp.Classes;

public class Sistema {
  String sistema;
  String slug;
  String lenguaje;
  String[] arraySistemas = {"us", "si"};
  String[] arrayLenguajes =  {"en", "es", "it", "pt"};

  public String[] getArraySistemas() {
    return arraySistemas;
  }

  public String[] getArrayLenguajes() {
    return arrayLenguajes;
  }

  public String getSistema() {
    return sistema;
  }

  public void setSistema(String sistema) {
    this.sistema = sistema;
    if(sistema.equals("si")){
      setSlug(" °C");
    } else if (sistema.equals("us")) {
      setSlug(" °F");
    }
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getLenguaje() {
    return lenguaje;
  }

  public void setLenguaje(String lenguaje) {
    this.lenguaje = lenguaje;
  }
}
