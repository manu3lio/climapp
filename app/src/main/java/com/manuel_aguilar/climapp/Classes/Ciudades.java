package com.manuel_aguilar.climapp.Classes;

public class Ciudades {
  String ID;
  String Ciudad;
  String Latitud;
  String Longitud;

  public String getID() {
    return ID;
  }

  public void setID(String ID) {
    this.ID = ID;
  }

  public String getCiudad() {
    return Ciudad;
  }

  public void setCiudad(String ciudad) {
    Ciudad = ciudad;
  }

  public String getLatitud() {
    return Latitud;
  }

  public void setLatitud(String latitud) {
    Latitud = latitud;
  }

  public String getLongitud() {
    return Longitud;
  }

  public void setLongitud(String longitud) {
    Longitud = longitud;
  }
}
