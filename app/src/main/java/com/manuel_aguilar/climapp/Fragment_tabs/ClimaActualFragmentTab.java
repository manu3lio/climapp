package com.manuel_aguilar.climapp.Fragment_tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.manuel_aguilar.climapp.Activity.PrincipalActivity;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.darkSkyForecast;
import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.sistema;
import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.textoHeader;


public class ClimaActualFragmentTab extends Fragment {
  DarkSkyForecast.DataClima currently = new DarkSkyForecast.DataClima();
  Unbinder unbinder;
  View view;

  @BindView(R.id.IV_logo_actual) ImageView IV_logo_actual;
  @BindView(R.id.TV_temperature_actual) TextView TV_temperature_actual;
  @BindView(R.id.TV_grados_actual) TextView TV_grados_actual;
  @BindView(R.id.TV_summary_actual) TextView TV_summary_actual;
  @BindView(R.id.TV_apparentTemperature_actual) TextView TV_apparentTemperature_actual;
  @BindView(R.id.TV_humidity_actual) TextView TV_humidity_actual;
  @BindView(R.id.TV_precipProbability_actual) TextView TV_precipProbability_actual;

  @BindView(R.id.TV_uvIndex_actual) TextView TV_uvIndex_actual;
  @BindView(R.id.TV_visibility_actual) TextView TV_visibility_actual;
  @BindView(R.id.TV_windSpeed_actual) TextView TV_windSpeed_actual;

  @BindView(R.id.SwipeRefresh) SwipeRefreshLayout SwipeRefresh;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    currently = darkSkyForecast.getCurrently();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_clima_actual_fragment_tab, container, false);
    unbinder = ButterKnife.bind(this, view);

    textoHeader.setText(darkSkyForecast.getCiudad());
    setIcon(currently.getIcon());
    setInfoActual(currently);
    setSwipeContainer();

    return view;
  }

  public void setSwipeContainer(){
    //Listener del swipeContainer
    SwipeRefresh.setOnRefreshListener(() -> ((PrincipalActivity)getActivity()).hacerRequestDarkSky_Fragments(SwipeRefresh));

    // Colores mientras hace el SwipeRefresh
    SwipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorAccent));
  }

  void setIcon(String icon){
    switch (icon){
      case "clear-day":
        IV_logo_actual.setImageDrawable(getContext().getResources().getDrawable(R.drawable.clear_day));
        break;
      case "clear-night":
        IV_logo_actual.setImageDrawable(getContext().getResources().getDrawable(R.drawable.clear_night));
        break;
      case "rain":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.rain));
        break;
      case "snow":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.snow));
        break;
      case "sleet":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.sleet));
        break;
      case "fog":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.fog));
        break;
      case "partly-cloudy-night":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.partly_cloudy_night));
        break;
      case "wind":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.wind));
        break;
      case "cloudy":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.cloudy));
        break;
      case "partly-cloudy-day":
        IV_logo_actual.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.partly_cloudy_day));
        break;
    }
  }

  void setInfoActual(DarkSkyForecast.DataClima currently){
    TV_temperature_actual.setText(currently.getTemperature() + " ");
    TV_grados_actual.setText(sistema.getSlug());

    TV_summary_actual.setText(currently.getSummary());
    TV_apparentTemperature_actual.setText(currently.getApparentTemperature()+ " " + sistema.getSlug());

    TV_humidity_actual.setText((currently.getHumidity() * 100) + " %");
    TV_precipProbability_actual.setText(Math.round(currently.getPrecipProbability() * 100) + " %");

    String vel = (sistema.getSistema().equals("us") ? "mph":"km/h");

    TV_uvIndex_actual.setText(currently.getUvIndex() +"");
    TV_visibility_actual.setText(currently.getVisibility() +"");
    TV_windSpeed_actual.setText(currently.getWindSpeed() +" " + vel);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
