package com.manuel_aguilar.climapp.Fragment_tabs;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.manuel_aguilar.climapp.Activity.PrincipalActivity;
import com.manuel_aguilar.climapp.Adapters.ClimaSemana_List_Adapter;
import com.manuel_aguilar.climapp.Classes.DarkSkyForecast;
import com.manuel_aguilar.climapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.darkSkyForecast;
import static com.manuel_aguilar.climapp.Activity.PrincipalActivity.mLastClickTime;
import static com.manuel_aguilar.climapp.Utils.ActivityBase.TAG;
import static com.manuel_aguilar.climapp.Utils.ActivityBase.gson;


public class ClimaSemanaFragmentTab extends Fragment {
  DarkSkyForecast.DataClimaInfo daily = new DarkSkyForecast.DataClimaInfo();
  Unbinder unbinder;
  View view;
  @BindView(R.id.LV_listaSemana) ListView LV_listaSemana;
  @BindView(R.id.TV_summary_semana) TextView TV_summary_semana;
  @BindView(R.id.IV_icon_semana) ImageView IV_icon_semana;
  @BindView(R.id.SwipeRefresh) SwipeRefreshLayout SwipeRefresh;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    daily = darkSkyForecast.getDaily();
    Log.d(TAG, "daily: " + gson.toJson(daily));
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_clima_semana_fragment_tab, container, false);
    unbinder = ButterKnife.bind(this, view);

    setInfoActual(daily);
    setlistSemana(daily.getData());
    setSwipeContainer_semana();
    return view;
  }

  public void setSwipeContainer_semana(){
    //Listener del swipeContainer
    SwipeRefresh.setOnRefreshListener(() -> ((PrincipalActivity)getActivity()).hacerRequestDarkSky_Fragments(SwipeRefresh));

    // Colores mientras hace el SwipeRefresh
    SwipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorAccent));
  }

  public Drawable getIcon(String icon) {
    Drawable drawable = null;
    switch (icon){
      case "clear-day":
        drawable = (getContext().getResources().getDrawable(R.drawable.clear_day));
        break;
      case "clear-night":
        drawable = (getContext().getResources().getDrawable(R.drawable.clear_night));
        break;
      case "rain":
        drawable = (getContext().getResources().getDrawable(R.drawable.rain));
        break;
      case "snow":
        drawable = (getContext().getResources().getDrawable(R.drawable.snow));
        break;
      case "sleet":
        drawable = (getContext().getResources().getDrawable(R.drawable.sleet));
        break;
      case "fog":
        drawable = (getContext().getResources().getDrawable(R.drawable.fog));
        break;
      case "partly-cloudy-night":
        drawable = (getContext().getResources().getDrawable(R.drawable.partly_cloudy_night));
        break;
      case "wind":
        drawable = (getContext().getResources().getDrawable(R.drawable.wind));
        break;
      case "cloudy":
        drawable = (getContext().getResources().getDrawable(R.drawable.cloudy));
        break;
      case "partly-cloudy-day":
        drawable = (getContext().getResources().getDrawable(R.drawable.partly_cloudy_day));
        break;
    }
    return drawable;
  }

  void setInfoActual(DarkSkyForecast.DataClimaInfo daily) {
    TV_summary_semana.setText(daily.getSummary());
    Log.d(TAG, "daily.getIcon(): " +daily.getIcon());
    IV_icon_semana.setImageDrawable(getIcon(daily.getIcon()));
  }

  public void setSwipeContainer() {
    SwipeRefresh.setOnRefreshListener(() -> {
      //hacerRequest_colaboradorConvencion();
      SwipeRefresh.setRefreshing(false);
    });
    SwipeRefresh.setColorSchemeColors(getResources().getIntArray(R.array.arrayColoresSwipe));
  }

  void setlistSemana(ArrayList<DarkSkyForecast.DataClima> data) {
    // Creacion de la vista
    ClimaSemana_List_Adapter ListaAdapter = new ClimaSemana_List_Adapter(getContext(),darkSkyForecast,  data);
    LV_listaSemana.setAdapter(ListaAdapter);
    LV_listaSemana.setClickable(true);
    LV_listaSemana.setOnItemClickListener((arg0, arg1, position, arg3) -> {
      if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
        return;
      }
      mLastClickTime = SystemClock.elapsedRealtime();
      //Obtener posicion del Objeto
      Object o = LV_listaSemana.getItemAtPosition(position);
      String json = gson.toJson(o);
      DarkSkyForecast.DataClima dataClima = gson.fromJson(json, DarkSkyForecast.DataClima.class);

      Log.d(TAG, "dataClima: "+ gson.toJson(dataClima));
    });

    // Override al controlador del Scroll de la LV_listaSemana para solo refrescar cuando esté en el top de la LV_listaSemana
    LV_listaSemana.setOnScrollListener(new AbsListView.OnScrollListener() {
      private boolean scrollEnabled;

      @Override
      public void onScrollStateChanged(AbsListView view, int scrollState) {
      }

      @Override
      public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int topRowVerticalPosition = (LV_listaSemana == null || LV_listaSemana.getChildCount() == 0) ? 0 : LV_listaSemana.getChildAt(0).getTop();
        boolean newScrollEnabled = (firstVisibleItem == 0 && topRowVerticalPosition >= 0) ? true : false;
        if (null != SwipeRefresh && scrollEnabled != newScrollEnabled) {
          // llego a inicio, refresca
          SwipeRefresh.setEnabled(newScrollEnabled);
          scrollEnabled = newScrollEnabled;
        }
      }
    });
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

}
